﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltWheelchair : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<int> CloneIntegerList(List<int> src)
    {
        List<int> cloned = new List<int>(src);
        return cloned;
    }

    public int CleverRandomInt(int min, int max, int forbidden=-1, float random_interval_percent=0.5f)
    {
        if (random_interval_percent > 1)
        {
            random_interval_percent = 1;
        }
        if (random_interval_percent < 0)
        {
            random_interval_percent = 0;
        }
        float fmax = max;
        float fmin = min;
        float size = fmax - fmin;
        float m = (fmax+fmin)/2;
        float r1 = m - size / 2 * random_interval_percent;
        float r2 = m + size / 2 * random_interval_percent;
        int r = 0;
        do
        {
            r = (int)Random.Range(r1, r2);
        } while (r == forbidden);
        Debug.Log(
            "CleverRandomInt: min=" + min + ", max=" + max + ", forbidden=" + forbidden + ", rand_interval=" + random_interval_percent
            + ", fmax=" + fmax + ", fmin=" + fmin + ", size=" + size + ", m=" + m + ", r1=" + r1 + ", r2=" + r2 + ", r=" + r
        );
        
        return r;
    }
}
